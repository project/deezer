-- SUMMARY --

Allows users to login on Drupal website using Deezer account info.
In future more features (like player and playlist manipulations) will be added.

If you have any thoughts on the module — feel free to contact the maintainer.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Go to http://developers.deezer.com/myapps and create own application.

* Go to http://YOURSITE/admin/config/media/deezer and enter your Deezer app's
  info.

* Go to http://YOURSITE/admin/structure/block and place "Deezer Login" block
  where you want it to be.


-- CONTACT --

Current maintainers:
* Vasiliy Grotov (vgrotov) - http://drupal.org/user/574988
